#ifdef _MSC_VER
#define _CRT_SECURE_NO_DEPRECATE
#endif

#include<cstdio>
#include<unordered_map>
#include<vector>
#include<algorithm>

//non-solution to https://adventofcode.com/2018/day/4
//day04Input.txt comes from https://adventofcode.com/2018/day/4/input

int to_day_of_year ( int month, int day ) {
	return (100 * month) + day; //Considered doing this right but this is good enough for sorting
}

struct Log {
	int day_of_year;
	int minute;
	int id;
	//Assumes sleep/wake events alternate, so doesn't save a type

	Log ( int day, int min, int id ) : day_of_year ( day ), minute ( min ), id ( id ) { };

	bool operator<( const Log &rhs ) const {
		if ( day_of_year < rhs.day_of_year ) return true;
		else if ( rhs.day_of_year < day_of_year ) return false;
		else return minute < rhs.minute;
	}
};

struct Nap {
	int id;
	int start;
	int end;

	Nap ( int id, int start, int end ) : id ( id ), start ( start ), end ( end ) { };
};

void day04 ( ) {
	int month, day, hour, minute, id;
	char c;
	std::unordered_map<int, int> sleep_times = { };
	std::vector<Log> logs = { };
	std::vector<Nap> naps = { };

	FILE *input = fopen ( "day04Input.txt", "r" );
	if ( input == nullptr ) {
		printf ( "Failed to open input file\n" );
		return;
	}

	while ( fscanf ( input, "[1518-%d-%d %d:%d] ", &month, &day, &hour, &minute ) != EOF ) {
		if ( hour == 23 ) {
			minute = -1;
			day++;
		}
		fscanf ( input, "%c", &c );
		if ( c == 'G' ) {
			fscanf ( input, "uard #%d begins shift\n", &id );
		} else if ( c == 'f' ) {
			fscanf ( input, "alls asleep\n" );
			id = 0;
		} else {
			fscanf ( input, "akes up\n" );
			id = 0;
		}

		Log l ( to_day_of_year ( month, day ), minute, id );
		logs.push_back ( l );
	}

	std::sort ( logs.begin ( ), logs.end ( ) );
	
	for ( unsigned int i = 0; i < logs.size ( ); ) {
		if ( logs[ i ].id > 0 ) {
			id = logs[ i ].id;
			i++;
			while ( i < logs.size ( ) && logs[ i ].id == 0 ){
				int end;
				int start = logs[ i ].minute;
				i++;
				if ( i < logs.size ( ) && logs[ i ].id == 0 ) {
					end = logs[ i ].minute;
					i++;
				} else {
					end = 60;
				}
				sleep_times[ id ] += end - start;
				naps.push_back ( Nap ( id, start, end ) );
			}
		}
		else {
			printf ( "malformed logs\n" );
			break;
		}
	}

	int cur_id = -1, cur_max = -1;
	for ( auto elf : sleep_times ) {
		if ( elf.second > cur_max ) {
			cur_id = elf.first;
			cur_max = elf.second;
		}
	}

	int minutes[ 60 ] = { 0 };
	for ( Nap n : naps ) {
		if ( n.id != cur_id ) continue;
		for ( int i = n.start; i < n.end; i++ ) {
			minutes[ i ]++;
		}
	}

	cur_max = minutes[ 0 ];
	int best_minute = 0;
	for ( int i = 0; i < 60; i++ ) {
		if ( minutes[ i ] > cur_max ) {
			cur_max = minutes[ i ];
			best_minute = i;
		}
	}

	printf ( "ID is: %d\nminute is: %d\n", cur_id, best_minute );

	printf ( "minutes:\n" );
	for ( int i = 0; i < 6; i++ ) {
		for ( int j = 0; j < 10; j++ ) { 
			printf ( "%2d ", minutes[ 10 * i + j ] );
		}
		printf ( "\n" );
	}

	int best_id = 0;
	cur_id = 0;
	cur_max = 0;
	for ( auto elf : sleep_times ) {
		cur_id = elf.first;
		int arr[ 60 ] = { 0 };
		for ( Nap n : naps ) { 
			if ( n.id != cur_id ) continue;
			for ( int i = n.start; i < n.end; i++ ) {
				arr[ i ]++;
			}
		}

		int tmp_max = arr[ 0 ];
		int tmp_best_minute = 0;
		for ( int i = 0; i < 60; i++ ) {
			if ( arr[ i ] > tmp_max ) {
				tmp_max = arr[ i ];
				tmp_best_minute = i;
			}
		}

		if ( tmp_max > cur_max ) {
			cur_max = tmp_max;
			best_id = cur_id;
			best_minute = tmp_best_minute;
		}
	}

	printf ( "best id is: %d\nbest minute is: %d\n", best_id, best_minute );
}