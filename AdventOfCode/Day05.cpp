#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include<cstdio>
#include<forward_list>
#include<math.h>
#include<cctype>
#include<limits>

void day05 ( ) {
	char c;
	int count;
	int best_count = std::numeric_limits<int>::max ( );
	int best_char = 'a';
	std::forward_list<char> polymer = { '!' };

	FILE* input = fopen ( "Day05Input.txt", "r" );
	if ( input == nullptr ) {
		printf ( "failed to open input file\n" );
		return;
	}

	count = 0;
	while ( fscanf ( input, "%c", &c ) != EOF ) {
		if ( !std::isalpha ( c ) ) continue;
		if ( std::abs ( c - polymer.front ( ) ) == 32 ) {
			polymer.pop_front ( );
			count--;
		} else {
			polymer.push_front ( c );
			count++;
		}
	}

	printf ( "count is: %d\n", count );

	for ( char rem = 'A'; rem <= 'Z'; rem++ ) {
		std::forward_list<char> tmp = { '!' };
		count = 0;
		for ( char mer : polymer ) {
			if ( !std::isalpha ( mer ) || ( mer - rem ) % 32 == 0 ) continue;
			if ( std::abs ( mer - tmp.front ( ) ) == 32 ) {
				tmp.pop_front ( );
				count--;
			} else {
				tmp.push_front ( mer );
				count++;
			}
		}
		if ( count < best_count ) {
			best_count = count;
			best_char = rem;
		}
	}

	printf ( "best length is: %d\n", best_count );
	printf ( "best char is: %c\n", best_char );
}