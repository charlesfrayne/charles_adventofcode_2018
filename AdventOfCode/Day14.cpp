#include<cstdio>
#include<vector>
using std::vector;

constexpr auto num_recipes = 260321;

void day14 ( ) {
	vector<int> recipes = { 3, 7 };
	int elf1 = 0;
	int elf2 = 1;

	while ( recipes.size ( ) < num_recipes + 10 ) {
		int sum = recipes[ elf1 ] + recipes[ elf2 ];
		if ( sum >= 10 )
			recipes.push_back ( 1 );
		recipes.push_back ( sum % 10 );

		elf1 = ( elf1 + 1 + recipes[ elf1 ] ) % recipes.size ( );
		elf2 = ( elf2 + 1 + recipes[ elf2 ] ) % recipes.size ( );
	}

	printf ( "target is: " );
	for ( int i = 0; i < 10; i++ ) {
		printf ( "%d", recipes[ num_recipes + i ] );
	} printf ( "\n" );
}

void day14_part_two ( ) {
	vector<int> recipes = { 3, 7 };
	int elf1 = 0;
	int elf2 = 1;
	unsigned long long rolling_sum = 0;
	int i = 0;

	while ( rolling_sum != num_recipes ) {
		int sum = recipes[ elf1 ] + recipes[ elf2 ];
		if ( sum >= 10 )
			recipes.push_back ( 1 );
		recipes.push_back ( sum % 10 );

		elf1 = ( elf1 + 1 + recipes[ elf1 ] ) % recipes.size ( );
		elf2 = ( elf2 + 1 + recipes[ elf2 ] ) % recipes.size ( );

		rolling_sum = ( ( rolling_sum % 100000 ) * 10 ) + recipes[ i ];
		i++;
	}

	printf ( "number of recipes before target is: %d\n", i - 6 );
}