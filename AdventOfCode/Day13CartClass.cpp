#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "Day13CartClass.h"
using Day13::Turn;
using Day13::Dir;
using Day13::Track;
using Day13::Cart;
#include<utility>
using std::pair;
#include<exception>
using std::exception;
#include<array>
using std::array;

//not sure if this is obvious enough behavior for overloaded ++
Turn operator++( Turn& orig ) {
	orig = (Turn) ( orig == 1 ? -1 : orig + 1 );
	return orig;
}

Turn operator++( Turn& orig, int ) {
	Turn ret = orig;
	++orig;
	return ret;
}

Dir make_turn ( const Dir initial_dir, const Turn turn_type ) {
	return (Dir) ( ( initial_dir + turn_type + 4 ) % 4 );
}

Dir follow_curve ( const Dir initial_dir, const Track curve ) {
	Dir new_dir;
	switch ( curve ) {
		case Track::Hack:
			new_dir = (Dir) ( 3 - initial_dir );
			break;
		case Track::Slash:
			new_dir = (Dir) ( initial_dir ^ 1 );
			break;
		default:
			throw exception ( "tried to follow a curve that wasn't curved\n" );
	}

	return new_dir;
}

array<array<Track, track_size>, track_size> Cart::tracks;

pair<int, int> Cart::dest ( ) const {
	int new_x = _x, new_y = _y;
	switch ( _dir ) {
		case Up:
			new_y--;
			break;
		case Down:
			new_y++;
			break;
		case Left:
			new_x--;
			break;
		case Right:
			new_x++;
			break;
	}
	return std::pair<int, int> ( new_x, new_y );
}

void Cart::turn ( const Track t ) {
	switch ( t ) {
		case Vert:
			if ( _dir == Right || _dir == Left )
				derail ( );
			break;
		case Flat:
			if ( _dir == Up || _dir == Down )
				derail ( );
			break;
		case Hack:
		case Slash:
			_dir = follow_curve ( _dir, t );
			break;
		case Cross:
			_dir = make_turn ( _dir, _turn_dir++ );
			break;
		case None:
			derail ( );
			break;
	}
}

void Cart::derail ( ) const {
	char* buf = new char[ 100 ];
	sprintf ( buf, "Cart went off the rails at %d, %d with direction %d", _x, _y, _dir );
	throw exception ( buf );
}

Track Cart::lookup_dest ( const pair<int, int> coords ) const {
	return tracks[ coords.second ][ coords.first ];
}

int Cart::move ( ) {
	auto dest = this->dest ( );
	Track t = lookup_dest ( dest );
	
	_x = dest.first; _y = dest.second;
	this->turn ( t );

	return ( _y * track_size ) + _x;
}

bool Cart::operator<( const Cart& rhs ) const {
	if ( _y < rhs._y )
		return true;
	else if ( _y == rhs._y )
		return _x < rhs._x;
	else
		return false;
}

bool Cart::operator==( const Cart& rhs ) const {
	return ( _x == rhs._x ) && ( _y == rhs._y );
}

bool Cart::operator>( const Cart& rhs ) const {
	return rhs < *this;
}