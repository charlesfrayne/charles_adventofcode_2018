#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include<vector>
#include<cstdio>

class node {
public:
	std::vector<node> children = { };
	std::vector<int> data = { };
};

int read_tree ( const std::vector<int> &input, int &pos, node &n ) {
	int num_children = input[ pos++ ];
	int num_metadata = input[ pos++ ];

	int sum = 0;
	for ( int i = 0; i < num_children; i++ ) {
		node child = { };
		sum += read_tree ( input, pos, child );
		n.children.push_back ( child );
	}

	for ( int i = 0; i < num_metadata; i++ ) {
		int x = input[ pos++ ];
		n.data.push_back ( x );
		sum += x;
	}
	return sum;
}

int eval_tree ( const node n ) {
	int sum = 0;
	if ( n.children.empty ( ) ) {
		for ( int d : n.data ) sum += d;
		return sum;
	}

	for ( int d : n.data ) {
		if ( d < 1 || d > n.children.size ( ) ) continue;
		sum += eval_tree ( n.children[ d - 1 ] );
	}
	return sum;
}

void day08 ( ) {
	int n;
	std::vector<int> input_vector = { };
	node root = { };

	FILE *input = fopen ( "Day08Input.txt", "r" );
	if ( input == nullptr ) {
		printf ( "failed to open input file\n" );
		return;
	}

	while ( fscanf ( input, "%d", &n ) != EOF ) {
		input_vector.push_back ( n );
	}

	int helper_zero = 0;
	int sum = read_tree ( input_vector, helper_zero, root );

	printf ( "sum is: %d\n", sum );

	int value = eval_tree ( root );
	printf ( "value is: %d\n", value );
}