#define _CRT_SECURE_NO_DEPRECATE
#include<iostream>
#include<fstream>
#include<vector>
#include<string>
using std::ifstream;
using std::vector;
using std::string;

void check_letters ( string, int&, int& );

void day02 ( ) {
	std::vector<string> ids = { };
	string id;
	int num_twos = 0, num_threes = 0;

	ifstream input ( "Day02Input.txt" );
	if ( !input ) {
		printf ( "Failed to open file\n" );
		return;
	}

	while ( std::getline( input, id ) ) {
		ids.push_back ( id );
		check_letters ( id, num_twos, num_threes );
	}

	printf ( "Checksum is %d\n", num_twos * num_threes );

	printf ( "Close strings:\n" );

	for ( unsigned int i = 0; i < ids.size ( ) - 1; i++ ) {
		for ( unsigned int j = i + 1; j < ids.size ( ); j++ ) {
			int flag = 0;
			for ( int pos = 0; pos < 26; pos++ ) {
				if ( ids[ i ][ pos ] != ids[ j ][ pos ] ) {
					if ( flag ) {
						flag++;
						break;
					}
					else flag++;
				}
			}
			if ( flag == 1 ) {
				std::cout << ids[ i ] << "\n456\n" << ids[ j ] << std::endl;
			}
		}
	}
}

void check_letters ( string id, int &num_twos, int &num_threes ) {
	int alph[ 26 ] = { 0 };
	bool has_twos = false, has_threes = false;

	for ( char c : id ) {
		alph[ c - 97 ]++;
	}
	for ( int n : alph ) {
		if ( n == 2 ) has_twos = true;
		if ( n == 3 ) has_threes = true;
	}

	if ( has_twos ) num_twos++;
	if ( has_threes ) num_threes++;

	return;
}
