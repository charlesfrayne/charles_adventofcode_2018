#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

//constexpr auto infile = "Day09Input.txt";
constexpr auto num_players = 404;
constexpr auto num_marbles = 7185200;
//constexpr auto num_players = 10;
//constexpr auto num_marbles = 25;

#include<array>
#include<vector>
#include<limits>

struct Marble {
	int number;
	Marble *next;
	Marble *prev;

	Marble ( int num, Marble *n, Marble *p ) : number ( num ), next ( n ), prev ( p ) { };
};

Marble* add_after ( int n, Marble *m ) {
	Marble* ret = new Marble ( n, m->next, m );
	m->next = ret;
	ret->next->prev = ret;
	return ret;
}

int remove_before ( Marble *m ) {
	Marble* tmp = m->prev;
	int ret = tmp->number;
	m->prev = tmp->prev;
	m->prev->next = m;
	delete tmp;
	return ret;
}

struct BiggishInt {
	unsigned long long small = 0;
	unsigned long long big = 0;

	BiggishInt ( unsigned long long s, unsigned long long b ) : small ( s ), big ( b ) { };
	BiggishInt ( ) : small ( 0 ), big ( 0 ) { };

	BiggishInt operator+( const BiggishInt &rhs ) const {
		unsigned long long b, s;
		unsigned long long rem = std::numeric_limits<unsigned long long>::max ( ) - small;
		if ( rem <= rhs.small ) {
			b = 1 + big + rhs.big;
			s = rhs.small - rem;
		} else {
			s = rhs.small + small;
			b = big + rhs.big;
		}
		return BiggishInt ( s, b );
	}

	BiggishInt operator+( const int &rhs ) const {
		unsigned long long b, s;
		unsigned long long rem = std::numeric_limits<unsigned long long>::max ( ) - small;
		if ( rem <= rhs ) {
			b = 1 + big;
			s = rhs - rem;
		} else {
			b = big;
			s = small + rhs;
		}
		return BiggishInt ( s, b );
	}

	void operator+=( const int &rhs ) {
		unsigned long long rem = std::numeric_limits<unsigned long long>::max ( ) - small;
		if ( rem <= rhs ) {
			big++;
			small = rhs - rem;
		} else {
			small += rhs;
		}
	}

	bool operator<( const BiggishInt &rhs ) const {
		if ( big < rhs.big )
			return true;
		else if ( big == rhs.big && small < rhs.small )
			return true;
		else
			return false;
	}
};

void day09 ( ) {
	std::array<int, num_players> scores = { 0 };
	std::vector<int> board = { 0, 2, 1 };


	int cur = 1;
	int size = 3;
	for ( int i = 3; i <= num_marbles; i++ ) {
		if ( i % 23 == 0 ) {
			int tmp = ( size + cur - 7 ) % size;
			scores[ i % num_players ] += i;
			scores[ i % num_players ] += board[ tmp ];
			board.erase ( board.begin ( ) + tmp );
			size--;
			cur = tmp % size;
		} else {
			cur = ( cur + 2 ) % size;
			size++;
			board.insert ( board.begin ( ) + cur, i );
		}
	}

	int max = 0;
	for ( int s : scores ) if ( s > max ) max = s;

	printf ( "max score is: %d\n", max );
}

void big_day09 ( ) {
	std::array<BiggishInt, num_players> scores;
	for ( auto &s : scores ) s = BiggishInt ( );
	Marble *cur = new Marble ( 1, NULL, NULL );
	cur->next = new Marble ( 0, NULL, cur );
	cur->prev = new Marble ( 2, cur, cur->next );
	cur->next->next = cur->prev;

	for ( int i = 3; i < num_marbles; i++ ) {
		if ( i % 23 == 0 ) {
			scores[ i % num_players ] += i;
			for ( int j = 0; j < 6; j++ ) cur = cur->prev;
			scores[ i % num_players ] += remove_before ( cur );
		} else {
			cur = add_after ( i, cur->next );
		}
	}

	BiggishInt max = BiggishInt ( 0, 0 );
	for ( BiggishInt b : scores ) if ( max < b ) max = b;

	printf ( "big component of max score is: %llu\n", max.big );
	printf ( "small component of max score is: %llu\n", max.small );
	printf ( "max unsigned long long is: %llu\n", std::numeric_limits<unsigned long long>::max ( ) );
}