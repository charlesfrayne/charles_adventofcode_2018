#pragma once
#include<array>
using std::array;
#include<utility>
using std::pair;

constexpr auto infile = "Day13Input.txt";
constexpr auto track_size = 150;

namespace Day13 {
	enum Track {
		None = 0,
		Vert = 1,
		Flat = 2,
		Cross = 3,
		Hack = 4,
		Slash = 5,
	};

	enum Dir {
		Up = 0,
		Right = 1,
		Down = 2,
		Left = 3,
	};

	enum Turn {
		LeftTurn = -1,
		Straight = 0,
		RightTurn = 1,
	};

	class Cart {
	private:
		Dir _dir;
		Turn _turn_dir;

		void derail ( ) const;
		void turn ( const Track t );
		pair<int, int> dest ( ) const;
		Track lookup_dest ( const pair<int, int> ) const;

	public:
		static array<array<Track, track_size>, track_size> tracks;
		int _x, _y;
		Cart ( int x, int y, Dir dir ) : _x ( x ), _y ( y ), _dir ( dir ), _turn_dir ( Turn::LeftTurn ) { };

		int move ( );

		bool operator<( const Cart& rhs ) const;
		bool operator==( const Cart& rhs ) const;
		bool operator>( const Cart& rhs ) const;
	};
}