#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include<cstdio>
#include<array>
#include<vector>

constexpr auto infile = "Day07Input.txt";

void day07 ( ) {
	char fst, nxt;
	std::array<std::array<bool, 26>, 26> reqs = { 0 };
	std::array<int, 26> num_blocking = { 0 };
	std::vector<int> complete = { };

	FILE *input = fopen ( infile, "r" );
	if ( input == nullptr ) {
		printf ( "failed to open input file\n" );
		return;
	}

	while ( fscanf ( input, "Step %c must be finished before step %c can begin.\n", &fst, &nxt ) != EOF ) {
		fst -= 'A'; nxt -= 'A';
		num_blocking[ nxt ]++;
		reqs[ fst ][ nxt ] = true;
	}

	std::array<int, 26> num_blocking_backup { num_blocking };

	int cur_task;
	do {
		cur_task = -1;
		for ( int i = 0; i < 26; i++ ) {
			if ( num_blocking[ i ] == 0 ) {
				cur_task = i;
				num_blocking[ i ] = -1;
				break;
			}
		}
		if ( cur_task == -1 ) break;
		complete.push_back ( cur_task );
		for ( int i = 0; i < 26; i++ ) {
			if ( reqs[ cur_task ][ i ] ) num_blocking[ i ]--;
		}
	} while ( cur_task != -1 );

	for ( int n : complete ) {
		printf ( "%c", n + 'A' );
	}
	printf ( "\n" );

	std::array<int, 26> times;
	for ( int i = 0; i < 26; i++ ) times[ i ] = 60 + i + 1;
	num_blocking = num_blocking_backup;

	int elapsed_time = -1;
	bool done = false;
	do {
		elapsed_time++;
		done = true;
		int elves_working = 0;
		for ( int i = 0; i < 26 && elves_working < 6; i++ ) {
			if ( num_blocking[ i ] == 0 ) {
				elves_working++;
				times[ i ]--;
				done = false;
			}
		}

		for ( int i = 0; i < 26; i++ ) {
			if ( times[ i ] == 0 ) {
				times[ i ] = -1;
				num_blocking[ i ] = -1;
				for ( int j = 0; j < 26; j++ ) {
					if ( reqs[ i ][ j ] ) num_blocking[ j ]--;
				}
			}
		}
	} while ( !done );

	printf ( "elapsed time is: %d\n", elapsed_time );
}