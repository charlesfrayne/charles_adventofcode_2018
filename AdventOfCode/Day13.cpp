#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "Day13CartClass.h"
using Day13::Cart;
using Day13::Track;
using Day13::Dir;

#include<cstdio>
#include<array>
using std::array;

#include<map>
using std::map;

void read_tracks ( FILE*,
				   array<array<Track, track_size>, track_size>&,
				   map<int, Cart*>& );
bool log_crash ( const int );

void day13 ( ) {
	map<int, Cart*> carts = { };

	FILE* input = fopen ( infile, "r" );
	if ( input == nullptr ) {
		printf ( "failed to open input file\n" );
		return;
	}

	read_tracks ( input, Cart::tracks, carts );

	bool crashed = false;
	while ( carts.size ( ) > 1 ) {
		map<int, Cart*> new_carts = { };
		for ( auto it = carts.begin ( ); it != carts.end ( ); ) {
			Cart* cart = it->second;
			int pos = cart->move ( );
			
			//If the new position is on an existing cart, erase it and don't save this cart, otherwise save new position
			if ( carts.count ( pos ) ) {
				//Check carts that haven't moved
				carts.erase ( pos );
				crashed = crashed || log_crash ( pos );
			} else if ( new_carts.count ( pos ) ) {
				//Check against carts that have already moved
				new_carts.erase ( pos );
				crashed = crashed || log_crash ( pos );
			} else {
				new_carts[ pos ] = cart;
			}
			//erase now that the cart has moved
			it = carts.erase ( it );
		}
		carts = new_carts;
	}

	int x = ( carts.begin ( )->first ) % track_size;
	int y = ( carts.begin ( )->first ) / track_size;
	printf ( "last remaining cart at x=%d and y=%d\n", x, y );
}

//Some rather verbose IO follows
void read_tracks ( FILE* input,
				   array<array<Track, track_size>, track_size>& tracks,
				   map<int, Cart*>& carts ) {
	for ( int y = 0; y < track_size; y++ ) {
		for ( int x = 0; x < track_size; x++ ) {
			char t;
			int pos = ( ( track_size * y ) + x );

			if ( fscanf ( input, "%c", &t ) != 1 ) {
				printf ( "expected character but no match\n" );
				return;
			}

			switch ( t ) {
				case '<':
					carts[ pos ] = new Cart ( x, y, Dir::Left );
					t = '-';
					break;
				case '>':
					carts[ pos ] = new Cart ( x, y, Dir::Right );
					t = '-';
					break;
				case '^':
					carts[ pos ] = new Cart ( x, y, Dir::Up );
					t = '|';
					break;
				case 'v':
					carts[ pos ] = new Cart ( x, y, Dir::Down );
					t = '|';
					break;
			}

			switch ( t ) {
				case '-':
					tracks[ y ][ x ] = Track::Flat;
					break;
				case '|':
					tracks[ y ][ x ] = Track::Vert;
					break;
				case '\\':
					tracks[ y ][ x ] = Track::Hack;
					break;
				case '/':
					tracks[ y ][ x ] = Track::Slash;
					break;
				case '+':
					tracks[ y ][ x ] = Track::Cross;
					break;
				default:
					printf ( "weird input found\n" );
				case ' ':
					tracks[ y ][ x ] = Track::None;
			}
		}

		char eol;
		if ( fscanf ( input, "%c", &eol ) == EOF ) {
			printf ( "unexpected EOF\n" );
			return;
		} else if ( eol != '\n' ) {
			printf ( "expected newline not found\n" );
			return;
		}
	}
}

bool log_crash ( const int pos ) {
	int first_crash_x = pos % track_size;
	int first_crash_y = pos / track_size;
	printf ( "first crash at x=%d and y=%d\n", first_crash_x, first_crash_y );
	return true;
}