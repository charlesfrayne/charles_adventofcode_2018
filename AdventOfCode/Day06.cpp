#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include<cstdio>
#include<utility>
#include<limits>
#include<queue>
#include<unordered_map>
#include<vector>
using std::vector;

//*
#define INFILE "Day06Input.txt"

#define XMIN 67
#define XMAX 357

#define YMIN 41
#define YMAX 353
//*/

/*
#define INFILE "Day06InputEasy.txt"

#define XMIN 1
#define XMAX 8

#define YMIN 1
#define YMAX 9
//*/

#define XDIM (XMAX-XMIN+1)
#define YDIM (YMAX-YMIN+1)

typedef std::pair<int, int> Cell;
typedef std::pair<int, int> Coord;

void day06 ( ) {
	int x, y, n = 0;
	std::unordered_map<int, int> areas = { };
	
	vector<vector<int>> dists;
	dists.resize ( XDIM );
	for ( int i = 0; i < XDIM; i++ ) {
		dists[ i ].resize ( YDIM );
		for ( int j = 0; j < YDIM; j++ ) dists[ i ][ j ] = 0;
	}

	vector<vector<Cell>> area = { };
	area.resize ( XDIM );
	for ( int i = 0; i < XDIM; i++ ) {
		area[ i ].resize ( YDIM );
		for ( int j = 0; j < YDIM; j++ ) area[ i ][ j ] = Cell ( std::numeric_limits<int>::max ( ), -1 );
	}

	FILE *input = fopen ( INFILE, "r" );
	if ( input == nullptr ) {
		printf ( "Failed to open file\n" );
		return;
	}

	while ( fscanf ( input, "%d, %d\n", &x, &y ) != EOF ) {
		n++;
		x -= XMIN;
		y -= YMIN;
		for ( int i = 0; i < XDIM; i++ ) {
			for ( int j = 0; j < YDIM; j++ ) dists[ i ][ j ] += abs ( x - i ) + abs ( y - j );
		}

		std::queue<Coord> q = { };
		q.push ( Coord ( x, y ) );
		while ( !q.empty() ) {
			Coord cur = q.front ( ); q.pop ( );
			int a = cur.first;
			int b = cur.second;
			int d = abs ( x - a ) + abs ( y - b );

			if ( d < area[ a ][ b ].first ) {
				area[ a ][ b ].first = d;
				area[ a ][ b ].second = n;
				if ( a + 1 < XDIM ) q.push ( Coord ( a + 1, b ) );
				if ( a > 0 ) q.push ( Coord ( a - 1, b ) );
				if ( b + 1 < YDIM ) q.push ( Coord ( a, b + 1 ) );
				if ( b > 0 ) q.push ( Coord ( a, b - 1 ) );
			} else if ( d == area[ a ][ b ].first && n != area[ a ][ b ].second ) {
				area[ a ][ b ].second = -1;
			}
		}
	}

	/*FILE *output = fopen ( "Day06TestOutput.txt", "w" );
	for ( x = 0; x < XDIM; x++ ) {
		for ( y = 0; y < YDIM; y++ ) {
			fprintf ( output, "%3d ", area[ x ][ y ] );
		}
		fprintf ( output, "\n" );
	}*/

	for ( x = 0; x < XDIM; x++ ) {
		for ( y = 0; y < YDIM; y++ ) {
			areas[ area[ x ][ y ].second ]++;
		}
	}

	for ( x = 0; x < XDIM; x++ ) {
		areas[ area[ x ][ 0 ].second ] = 0;
		areas[ area[ x ][ YDIM - 1 ].second ] = 0;
	}
	for ( y = 0; y < YDIM; y++ ) {
		areas[ area[ 0 ][ y ].second ] = 0;
		areas[ area[ XDIM - 1 ][ y ].second ] = 0;
	}

	int max_area = 0;
	for ( auto a : areas ) {
		if ( a.second > max_area ) max_area = a.second;
	}

	printf ( "max area is: %d\n", max_area );

	int count = 0;
	for ( int i = 0; i < XDIM; i++ ) for ( int j = 0; j < YDIM; j++ ) if ( dists[ i ][ j ] < 10000 ) count++;

	printf ( "count is: %d\n", count );
}