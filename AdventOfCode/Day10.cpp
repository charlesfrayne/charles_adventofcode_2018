#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include<vector>
#include<utility>
#include<algorithm>
#include<cstdio>

constexpr auto infile = "Day10Input.txt";

typedef std::pair< int, int > pnt;
typedef std::pair< pnt, pnt > star;

void day10 ( ) {
	int xpos, ypos, xvel, yvel;
	std::vector < star > stars = { };
	
	FILE* input = fopen ( infile, "r" );
	if ( input == nullptr ) {
		printf ( "failed to open file\n" );
		return;
	}

	while ( fscanf ( input, "position=<%d,%d> velocity=<%d,%d>\n", &xpos, &ypos, &xvel, &yvel ) != EOF ) {
		star s ( pnt ( xpos + 10000 * xvel, ypos + 10000 * yvel ), pnt ( xvel, yvel ) );
		stars.push_back ( s );
	}

	int count = 10000;
	int xmax, ymax, xmin, ymin;
	bool done = false;
	do {
		xmax = -10000;
		xmin = 10000;
		ymax = -10000; int ymaxb = -10000;
		ymin = 10000; int yminb = 10000;

		for ( auto& s : stars ) {
			xmax = std::max ( xmax, s.first.first );
			xmin = std::min ( xmin, s.first.first );
			ymax = std::max ( ymax, s.first.second );
			ymin = std::min ( ymin, s.first.second );
			s.first.first += s.second.first;
			s.first.second += s.second.second;
			ymaxb = std::max ( ymaxb, s.first.second );
			yminb = std::min ( yminb, s.first.second );
		}
		done = ( ymaxb - yminb ) > ( ymax - ymin );
		count++;
	} while ( !done );
	count--;
	printf ( "count is: %d\n", count );

	for ( auto& s : stars ) {
		s.first.first -= s.second.first;
		s.first.second -= s.second.second;
	}

	std::vector<std::vector<char>> sky ( ( ymax - ymin + 1 ), std::vector<char> ( ( xmax - xmin + 1 ), ' ' ) );
	for ( const auto s : stars ) {
		sky[ s.first.second - ymin ][ s.first.first - xmin ] = '#';
	}

	for ( const auto s : sky ) {
		for ( const auto t : s )
			printf ( "%c", t );
		printf ( "\n" );
	}
}