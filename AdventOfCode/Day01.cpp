#define _CRT_SECURE_NO_DEPRECATE
#include<cstdio>
#include<vector>
#include<set>

void day01 ( ) {
	std::vector<int> freq = { };
	std::set<int> seen = { };
	int n, i, sum = 0;

	FILE *input = fopen ( "Day01Input.txt", "r" );
	if ( input == nullptr ) {
		printf ( "Failed to open file\n" );
		return;
	}

	while ( fscanf ( input, "%d\n", &n ) != EOF ) {
		freq.push_back ( n );
		sum += n;
	}

	printf ( "Sum is %d\n ", n );

	sum = 0;
	i = 0;
	while ( seen.count ( sum ) == 0 ) {
		seen.insert ( sum );
		sum += freq[ i++ % freq.size() ];
	}
	printf ( "First repeat is %d\n ", sum );
}