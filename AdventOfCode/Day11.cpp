#include<array>

constexpr auto serial_num = 7347;

int power_at ( int x, int y ) {
	int rack_id = x + 10;
	int power = rack_id * y;
	power += serial_num;
	power *= rack_id;
	power = ( ( power / 100 ) % 10 ) - 5;
	return power;
}

void day11 ( ) {
	std::array<std::array<int, 300>, 300> grid;
	for ( int x = 1; x <= 300; x++ )
		for ( int y = 1; y <= 300; y++ )
			grid[ x - 1 ][ y - 1 ] = power_at ( x, y );

	int max_power = 0;
	int max_x = 0, max_y = 0, max_s = 3;
	for ( int s = 3; s < 300; s++ ) {
		for ( int y = 0; y <= 300 - s; y++ ) {
			int power = 0;
			for ( int i = 0; i < s; i++ )
					for ( int j = 0; j < s; j++ )
						power += grid[ i ][ y + j ];
			if ( power > max_power ) {
					max_power = power;
					max_x = 1;
					max_y = y + 1;
					max_s = s;
			}

			for ( int x = 1; x <= 300 - s; x++ ) {
				for ( int i = 0; i < s; i++ ) {
					power += grid[ x + s - 1 ][ y + i ] - grid[ x - 1 ][ y + i ];
				}

				if ( power > max_power ) {
					max_power = power;
					max_x = x + 1;
					max_y = y + 1;
					max_s = s;
				}
			}
		}
	}
	printf ( "best coordinates are at: (%d, %d)\n", max_x, max_y );
}