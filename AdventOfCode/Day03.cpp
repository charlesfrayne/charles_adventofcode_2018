#define _CRT_SECURE_NO_DEPRECATE
#include<cstdio>
#include<vector>
#include<set>
#include<utility>
#include<algorithm>
using std::vector;
using std::set;
using std::pair;
using std::max;

struct Rect {
	int top;
	int bottom;
	int left;
	int right;
	Rect ( ) : top ( 0 ), bottom ( 0 ), left ( 0 ), right ( 0 ) { };
	Rect ( int top, int bottom, int left, int right ) : top ( top ), bottom ( bottom ), left ( left ), right ( right ) { };
};

void day03 ( ) {
	vector<Rect> rects = { };
	set<pair<int, int>> used = { };
	set<pair<int, int>> used_twice = { };
	int **cloth = new int*[ 1024 ];
	for ( int x = 0; x < 1024; x++ ) cloth[ x ] = new int[ 1024 ];
	for ( int x = 0; x < 1024; x++ ) for ( int y = 0; y < 1024; y++ ) cloth[ x ][ y ] = 0;
	int n, max_x = 0, max_y = 0;
	int num_overlap = 0;

	FILE *input = fopen ( "Day03Input.txt", "r" );
	if ( input == nullptr ) {
		printf ( "Failed to open file\n" );
		return;
	}

	while ( fscanf ( input, "#%d @ ", &n ) != EOF ) {
		int top, left, width, height;
		n = fscanf ( input, "%d,%d: %dx%d\n", &left, &top, &width, &height );
		rects.push_back ( Rect ( top, top + height, left, left + width ) );

		for ( int x = top; x < top + height; x++ ) {
			for ( int y = left; y < left + width; y++ ) {
				cloth[ x ][ y ]++;
			}
		}
	}

	for ( int x = 0; x < 1024; x++ ) {
		for ( int y = 0; y < 1024; y++ ) {
			if ( cloth[ x ][ y ] > 1 ) num_overlap++;
		}
	}
	printf ( "num overlap is %d\n", num_overlap );

	for ( Rect r : rects ) {
		bool good = true;
		for ( int i = r.top; i < r.bottom; i++ ) {
			for ( int j = r.left; j < r.right; j++ ) {
				if ( cloth[ i ][ j ] > 1 ) {
					good = false;
					break;
				}
			}
			if ( !good ) break;
		}
		if ( good ) {
			printf ( "%d,%d\n", r.left, r.top );
			break;
		}
	}
}