#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include<cstdio>
#include<array>
#include<vector>

constexpr auto infile = "Day12Input.txt";

void day12 ( ) {
	std::array<bool,32> rules;
	std::vector<bool> pots = { };
	int start = 0;
	char pot;

	FILE* input = fopen ( infile, "r" );
	if ( input == nullptr ) {
		printf ( "failed to open input file\n" );
		return;
	}

	while ( fscanf ( input, "%c", &pot ) != EOF ) {
		if ( pot == '\n' ) {
			fscanf ( input, " " );
			break;
		}

		pots.push_back ( pot == '#' );
	}

	for ( int i = 0; i < 32; i++ ) {
		char c1, c2, c3, c4, c5, res;
		fscanf ( input, "%c%c%c%c%c => %c\n", &c5, &c4, &c3, &c2, &c1, &res );
		int num = ( c1 == '#' ) | ( ( c2 == '#' ) << 1 ) | ( ( c3 == '#' ) << 2 ) | ( ( c4 == '#' ) << 3 ) | ( ( c5 == '#' ) << 4 );
		rules[ num ] = res == '#';
	}

	for ( int gen = 0; gen < 201; gen++ ) {
		for ( int i = 50; i < 250; i++ ) {
			if ( i < start ) {
				printf ( "." );
			} else if ( i - start < pots.size ( ) ) {
				if ( pots[ i - start ] )
					printf ( "#" );
				else
					printf ( "." );
			} else {
				printf ( "." );
			}
		}
		printf ( "\n" );
		std::vector<bool> tmp_pots = { };
		bool start_found = false;
		int val = 0;

		for ( int i = 0; i < pots.size ( ) + 4; i++ ) {
			bool plant = i < pots.size ( ) && pots[ i ];
			val = ( ( val & ( ~16 ) ) << 1 ) | plant;

			if ( rules[ val ] ) {
				if ( !start_found ) {
					start_found = true;
					start = start + i - 2;
				}
				tmp_pots.push_back ( true );
			} else if ( start_found ) {
				tmp_pots.push_back ( false );
			}
		}

		pots = tmp_pots;
	}

	int sum = 0;
	for ( int i = 0; i < pots.size ( ); i++ ) {
		if ( pots[ i ] ) sum += start + i;
	}

	printf ( "sum is: %d\n", sum );
}